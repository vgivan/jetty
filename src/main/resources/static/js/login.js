$(document).ready(function () {
    //Mostrar/ocultar contraseña
    $(".toggle-password").click(function () {
        $(this).toggleClass("glyphicon-eye-open glyphicon-eye-close");
        var input = $($(this).attr("toggle"));

        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
});


//Controlar alerta de error
function toggleAlert(msg) {
    $('#error-msg').text(msg);
    $('.alert').toggleClass('in out');
    $('#password').val('');
}

//  Mostrar/ocultar contraseña en javascript puro
/* function showpass() {
    let pass = document.getElementById("password");
    let picon = document.getElementById("picon");


    if (pass.type === 'password') {
        pass.type = 'text';
        picon.className = 'glyphicon glyphicon-eye-close';
    }
    else {
        pass.type = 'password';
        picon.className = 'glyphicon glyphicon-eye-open';
    }
} */