package com.jetty.example.jetty;

import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class JettyController implements ErrorController{
    @Autowired
    JettyService jettyService;

    @GetMapping("/index")
    public String index() {
        return "index";
    }

    @PostMapping("/login")
    public String validateLogin(@RequestParam("name") String name, @RequestParam("password") String pass) {
        String res = "";

        res = (jettyService.validateUser(name, pass)) ? "home" : "password";

        return res;
    }

    @RequestMapping("/error")
    public ModelAndView handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        ModelAndView mav = new ModelAndView("error");

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());

            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                mav.addObject("title", "La página solicitada no se encuentra en este servidor");
                mav.addObject("code", "Error 404");
                mav.addObject("description",
                        "La página solicitada puede no estar disponible, haber"
                                + "cambiado de dirección (URL) o no existir. Con frecuencia es"
                                + "debido a algún error al escribir la dirección en la"
                                + "página (URL). Compruebe de nuevo si es correcta.");
            } else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                mav.addObject("title", "Error interno del servidor");
                mav.addObject("code", "Error 500");
                mav.addObject("description",
                        "El servidor web encontró una condición"
                                + "inesperada que le impidió completar tu solicitud para"
                                + "acceder a la URL requerida.</p>" + "<p>Por favor intentalo más tarde.");
            } else {
                mav.addObject("title", "Error en la petición");
                mav.addObject("code", "");
                mav.addObject("description", "Ocurrio un error alprocesar la petición");
            }
        }else {
            mav.addObject("title", "Error en la petición");
            mav.addObject("code", "");
            mav.addObject("description", "Ocurrio un error alprocesar la petición");
        }

        return mav;
    }
}